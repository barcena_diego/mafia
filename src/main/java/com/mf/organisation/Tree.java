package com.mf.organisation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class Tree {

	private Node root = null;
	private Integer i = 0;
	
	private List<Integer> nodeIds = new ArrayList<Integer>();
	private Map<Integer, Node> nodes = new HashMap<Integer, Node>();
	
	private List<Integer> jailed = new ArrayList<Integer>();
	private List<Integer> deceased = new ArrayList<Integer>();
	
	private Integer generalId = 0;

	public Tree(){
		super();
		root = this.new Node();
	}
	//Begin Node
	public class Node implements Comparable<Node>{
		
		private Node parent;
		//When one member of the Mafia goes to jail we keep his parent id.
		private Integer parentNodeId;
		
		private MafiaMember mafioso;
		
		private Set<Node> children = new TreeSet<Node>();
		//When one member of the Mafia goes to jail we keep his children ids.
		private Set<Integer> childrenIds = new HashSet<Integer>();

		private Integer nodeLevel;
		private Integer nodeId;
		private Integer numberOfChildren = 0;

		public Node(){
	
			super();
			this.nodeId = generalId++;
			nodeIds.add(this.nodeId);
			nodes.put(this.nodeId,this);
		}
		
		public void addChildId(Integer childId) {
			this.childrenIds.add(childId);
		}
		
		public Set<Integer> getChildrenIds() {
			return childrenIds;
		}
		
		public Integer getParentNodeId() {
			return parentNodeId;
		}

		public void setParentNodeId(Integer parentNodeId) {
			this.parentNodeId = parentNodeId;
		}
		
		public boolean hasMoreThan50Subordinates(){
			return getNumberOfChildren() > 50;
		}
		
		public Integer getNumberOfChildren() {
			numberOfChildren = this.children.size();
			
			for(Node node : this.children)
				numberOfChildren += node.getNumberOfChildren();
			
			return numberOfChildren;
		}
		
		public Integer getNodeId() {
			return nodeId;
		}

		public Integer getNodeLevel() {
			return nodeLevel;
		}
		
		public void setNodeLevel(Integer nodeLevel) {
			this.nodeLevel = nodeLevel;
		}
		
		public void modifyNodeLevel(Integer nodeLevel) {
			this.nodeLevel = nodeLevel;
			
			if(this.mafioso!=null)
				this.mafioso.setLevel(nodeLevel);
			
			for(Node node : this.children){
				node.modifyNodeLevel(nodeLevel+1);
			}
		}
		
		public Node getParent() {
			return parent;
		}
		public void setParent(Node parent) {
			this.parent = parent;
		}
		public MafiaMember getMafioso() {
			return mafioso;
		}
		public void setMafioso(MafiaMember mafioso) {
			this.mafioso = mafioso;
		}
		
		private Set<Node> getChildren() {
			return children;
		}
		
		public void setChildren(Set<Node> children) {
			this.children.addAll(children);
		}
		
		public void addChild(Node child) {
			this.children.add(child);
		}

		public int compareTo(Node node) {
			int result = 0;
			if (this.mafioso.getAge()<node.getMafioso().getAge())
				result =  1;
			else if (this.mafioso.getAge()>node.getMafioso().getAge())
				result = -1;
			else if(this.mafioso.getAge()==node.getMafioso().getAge()){
				if(this.mafioso.getName().equals(node.getMafioso().getName()))
					result = 0;
				else
					result = this.nodeId.compareTo(node.getNodeId());
			}
			return result;
		}
		
		public boolean isChildrenEmpty(){
			return children.isEmpty();
		}
		
		public void removeFromParent(){
			if(this.parent!=null)
				this.parent.getChildren().remove(this);
		}		
		
		@Override
		public boolean equals(Object obj){
			return this.nodeId == ((Node)obj).getNodeId();
		}
		
		@Override
		public int hashCode(){
			return this.nodeId;
		}
		
		@Override
		public String toString(){
			return this.mafioso.getName();
		}
	
	}
	//End Node
	
	
	public Node getRoot() {
		return root;
	}
	
	public Integer getNodeId(Integer index){
		return this.nodeIds.get(index);
	}
	
	public Integer getNumberOfNodes(){
		return this.nodeIds.size();
	}
	
	public Node getNode(Integer nodeId){
		return this.nodes.get(nodeId);
	}
	
	public void printTree(Node node) {
		
		System.out.println("Name : " + node.getMafioso().getName());
		System.out.println("Age : "  + node.getMafioso().getAge());
		System.out.println("Level : " + node.getMafioso().getLevel());
		System.out.println("Id : " + node.getNodeId());
		System.out.println("Number Of Children : " + node.getNumberOfChildren());
		System.out.println("Parent : " + (node.getParent()!=null?node.getParent().toString():""));
		System.out.println("");
		
		for(Node childNode : node.getChildren()) 
			printTree(childNode);
	}
	
	public void printTree() {
		Node node = root;
		System.out.println("Name : " + node.getMafioso().getName());
		System.out.println("Age : "  + node.getMafioso().getAge());
		System.out.println("Level : " + node.getMafioso().getLevel());
		System.out.println("Id : " + node.getNodeId());
		System.out.println("Number Of Children : " + node.getNumberOfChildren());
		System.out.println("Parent : " + (node.getParent()!=null?node.getParent().toString():""));
		System.out.println("");
		
		for(Node childNode : node.getChildren()) 
			printTree(childNode);
	}
	
	public Node getOldestNodeInLevel(Node node, Node originNode, Node olderNode) {
		
		Integer level = originNode.getNodeLevel();
		
		if(level==0) 
			return null;
		
		Node oldestNode = null;
		
		if(level==1) {
			for(Node nodeFor: (TreeSet<Node>)node.getChildren()){
				if(nodeFor.getNodeId()==originNode.getNodeId())
					continue;
				
				oldestNode = nodeFor;
				break;
			}
			return oldestNode;
		}

		if(olderNode != null)
			oldestNode = olderNode;
		
		Node oldestNodeInParent = null;
	
		for(Node childNode : node.getChildren()) {
			if(childNode.getNodeLevel()==level-1) {
				oldestNodeInParent = ((TreeSet<Node>)childNode.getChildren()).first();
				
				if(oldestNodeInParent.getNodeId()==originNode.getNodeId())
					continue;
				
				System.out.println("Name : " + oldestNodeInParent.getMafioso().getName());
				System.out.println("Age : "  + oldestNodeInParent.getMafioso().getAge());
				System.out.println( "Level : " + oldestNodeInParent.getMafioso().getLevel());
				System.out.println("Id : " + oldestNodeInParent.getNodeId());
				System.out.println("Number Of Childen : " + oldestNodeInParent.getNumberOfChildren());
				System.out.println("Block: " + (++i));
				System.out.println("");

				if(oldestNode==null || oldestNode.getMafioso().getAge()<oldestNodeInParent.getMafioso().getAge()) {
					
					System.out.println("Name : " + oldestNodeInParent.getMafioso().getName());
					System.out.println("Age : "  + oldestNodeInParent.getMafioso().getAge());
					System.out.println( "Level : " + oldestNodeInParent.getMafioso().getLevel());
					System.out.println("Id : " + oldestNodeInParent.getNodeId());
					System.out.println("Number Of Childen : " + oldestNodeInParent.getNumberOfChildren());
					System.out.println("Oldest in Block Number...: " + (i));
					System.out.println("");
				
					oldestNode = oldestNodeInParent;	
					
					System.out.println("Oldest Node Age : "  + oldestNode.getMafioso().getAge());
				}
			}else {
				oldestNode = getOldestNodeInLevel(childNode,originNode, oldestNode);
			}
		}//for
		
		return oldestNode;
	}
	
	public Node getOldestChild(Node originNode) {
		return ((TreeSet<Node>)originNode.getChildren()).first();
	}
	
	private void interchangeChildrenChildSuccessor(Node successor, Node originNode){
		
		for(Node node : originNode.getChildren() ){
			originNode.addChildId(node.getNodeId());
			if(!node.equals(successor)){
				node.setParent(successor);
				
				successor.addChild(node);
			}
		}
		
		successor.modifyNodeLevel(successor.getNodeLevel()-1);
		originNode.getChildren().clear();
	}
	
	private void interchangeChildrenOldestInLevelSuccessor(Node successor, Node originNode){

		for(Node node : originNode.getChildren() ){
			originNode.addChildId(node.getNodeId());
			
			node.setParent(successor);
			successor.addChild(node);
		}
		originNode.getChildren().clear();
	}
	
	public void goingToJail(Node originNode, boolean isJailed){
		Node successor = null;
		try{
			if(originNode.getNumberOfChildren() == 0)
				return;

			successor = getOldestNodeInLevel(root,originNode,null);
			
			if (successor==null){
				successor = getOldestChild(originNode);
				successor.setParent(originNode.getParent());
				if(originNode.getParent()!=null)
					originNode.getParent().addChild(successor);
				
				interchangeChildrenChildSuccessor(successor, originNode);
				
				if(originNode.getNodeId()==0)
					this.root = successor;
					
			}else{
				System.out.println("Successor ***********************************");
				System.out.println("*********************************************");				
				System.out.println("Name : " + successor.getMafioso().getName());
				System.out.println("Age : "  + successor.getMafioso().getAge());
				System.out.println( "Level : " + successor.getMafioso().getLevel());
				System.out.println("Id : " + successor.getNodeId());
				System.out.println("Number Of Childen : " + successor.getNumberOfChildren());
				System.out.println("*********************************************");
				
				interchangeChildrenOldestInLevelSuccessor(successor,originNode);
			}
		}finally{
			
			if(originNode.getParent()!=null){
				originNode.removeFromParent();
				originNode.setParentNodeId(originNode.getParent().getNodeId());
				originNode.setParent(null);
			}
			
			//A member of the Mafia could be either jailed or deceased
			if(isJailed)
				jailed.add(originNode.getNodeId());
			else
				deceased.add(originNode.getNodeId());
			
			Node storedNode = nodes.get(originNode.getNodeId());
			
			System.out.println("Stored **************************************");
			System.out.println("*********************************************");
			System.out.println("Name : " + storedNode.getMafioso().getName());
			System.out.println("Age : "  + storedNode.getMafioso().getAge());
			System.out.println( "Level : " + storedNode.getMafioso().getLevel());
			System.out.println("Id : " + storedNode.getNodeId());
			System.out.println("Number Of Childen : " + storedNode.getNumberOfChildren());
			System.out.println("*********************************************");
		}
	}
	
	public void releasedFromPrison(Node releasedNode){
		Node storedParentNode = nodes.get(releasedNode.getParentNodeId());
		
		if(storedParentNode!=null){
			releasedNode.setParent(storedParentNode);
			storedParentNode.addChild(releasedNode);
		}else
			releasedNode.setParent(null);
		
		for(Integer storedChilId: releasedNode.getChildrenIds()){
			Node storedChildNode = nodes.get(storedChilId);
			releasedNode.addChild(storedChildNode);
			storedChildNode.removeFromParent();
			storedChildNode.setParent(releasedNode);
		}
		
		if(storedParentNode!=null)
			releasedNode.modifyNodeLevel(storedParentNode.getNodeLevel()+1);
		else{ 
			releasedNode.modifyNodeLevel(0);
			this.root = releasedNode;
		}
		
		jailed.remove((Object)releasedNode.getNodeId());
	}
	
	public void releasedFromPrisonId(Integer releasedNodeId){
		Node releasedNode = nodes.get(releasedNodeId);
		
		releasedFromPrison(releasedNode);
	}
	
	public boolean makeIdsChildrenOfId(Integer[] movingIds, Integer destinyId){
		
		Node destinyNode = nodes.get(destinyId);
		Node movingNode = null;
		
		
		for(Integer id : movingIds){
			movingNode = nodes.get(id);
			if(!validateMoveIsPossible(movingNode,destinyNode))
				return false;
			destinyNode.addChild(movingNode);
			movingNode.modifyNodeLevel(destinyNode.getNodeLevel()+1);
			movingNode.removeFromParent();
			movingNode.setParent(destinyNode);
			
		}
		
		return true;

	}
	
	private boolean validateMoveIsPossible(Node movingNode, Node destinyNode){
		boolean isPossible = true;
		if(movingNode.getNodeLevel()==0 || movingNode.equals(destinyNode))
			return false;
		
		for(Node node : movingNode.getChildren()){
			if(node.equals(destinyNode)){
				return false;
				
			}else{
				if(!(isPossible = validateMoveIsPossible(node, destinyNode)))
					return false;
			}
		}
		return isPossible;
	}
}
