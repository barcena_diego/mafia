package com.mf.organisation.test;

import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import com.mf.organisation.MafiaMember;
import com.mf.organisation.Tree;

public class MafiaTreeGenerator {
	
	private Random rn = new Random();
	public Integer finalNodeLevel = 4;
	public Integer numberOfChildren = 4;
	private Integer age;
	public Tree testTree = null;
	
	public void generateOrganisation() {
	
		MafiaMember capo = new MafiaMember();
		
		Integer AGE = 30;
		String NAME = "NAME0";
			
		capo.setAge(AGE);
		capo.setName(NAME);
		capo.setLevel(0);
		
		testTree = new Tree();
		Tree.Node root = testTree.getRoot();
		
		root.setParent(null);
		root.setMafioso(capo);
		root.setNodeLevel(0);
		root.setChildren(fillChildren(root));
		
		//This dummmy organisation is going to have one root member.
		//Each node can have up to n children
		//There can be up to n levels excluding the root. So n+1 altogether.
	}
	
	private Set<Tree.Node> fillChildren(Tree.Node parent) {
		
		Set<Tree.Node> children = new TreeSet<Tree.Node>();
		
		Tree.Node child = null;
		MafiaMember mafioso = null;
		
		Integer currentNodeLevel, organisationLevel;
		
		currentNodeLevel = organisationLevel = parent.getMafioso().getLevel()+1;

		for(int i=0; i<numberOfChildren;i++) {
			
			child = testTree.new Node();
			mafioso = new MafiaMember();
			
			//Age range from 30 to 100 years
			age = rn.nextInt(71) + 30;
			
			mafioso.setAge(age);
			mafioso.setName(parent.getMafioso().getName().concat(String.valueOf(i)));
			mafioso.setLevel(organisationLevel);
			
			child.setParent(parent);
			child.setMafioso(mafioso);
			child.setNodeLevel(currentNodeLevel);
			
			if(currentNodeLevel<finalNodeLevel)
				child.setChildren(fillChildren(child));

			children.add(child);
		}
		return children;
	}
}
