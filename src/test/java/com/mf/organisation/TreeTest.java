package com.mf.organisation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.runners.MockitoJUnitRunner;

import java.util.Random;

import com.mf.organisation.test.MafiaTreeGenerator;

@RunWith(MockitoJUnitRunner.class)
public class TreeTest {
	private static Random rn = new Random();
	
	private final Integer AGE = 30; 
	private final String NAME = "Level0";
	private Integer nodeIdIndex;
	
	@Before
	public void setUp() throws Exception {

	}
	
	@Test
	public void givensTree_TreeRootIsNotnull(){
		//We set the values for the member of the Mafia
		MafiaMember capo = new MafiaMember();
		capo.setAge(this.AGE);
		capo.setName(this.NAME);
		
		//We create a new Tree
		Tree tree =new Tree();
		
		Tree.Node root = tree.getRoot();
		
		root.setParent(null);
		root.setMafioso(capo);
	
		assertNotNull(tree.getRoot());
		assertEquals(this.AGE, tree.getRoot().getMafioso().getAge());
		assertEquals(this.NAME, tree.getRoot().getMafioso().getName());
		assertTrue(tree.getRoot().isChildrenEmpty());
		assertNull(tree.getRoot().getParent());

	}
	
	@Test
	public void givensOneMafiosoGoingToJailAndOneChildIsTheNewBossAndReleased(){
		
		MafiaMember capo = new MafiaMember();
		boolean enabled = true;
		
		//Givens
		if(enabled){
			capo = new MafiaMember();
			capo.setAge(this.AGE);
			capo.setName(this.NAME);
			//We generate a dummy tree with 4 levels and 1 child per node
			MafiaTreeGenerator mafiaTreeGenerator = new MafiaTreeGenerator();
			mafiaTreeGenerator.finalNodeLevel = 4;
			mafiaTreeGenerator.numberOfChildren = 1;
			mafiaTreeGenerator.generateOrganisation();
			Tree tree = mafiaTreeGenerator.testTree;
			
			assertNotNull(tree);
			//
			Tree.Node root = tree.getRoot();
			
			assertNotNull(root);
			
			System.out.println("Test 2 givensOneMafiosoGoingToJailAndOneChildIsTheNewBossAndReleased");
			System.out.println("Initial Tree ********************************");
			System.out.println("*********************************************");			
			tree.printTree();
			System.out.println("*********************************************");
			System.out.println("");
			System.out.println("");
			
			//nodeIdIndex = rn.nextInt(tree.getNumberOfNodes());
			nodeIdIndex = 2;
			
			System.out.println("Index: &&& " + nodeIdIndex + " " + tree.getNodeId(nodeIdIndex) + " " + tree.getNumberOfNodes() + " &&&");
			System.out.println("");
			
			Tree.Node selectedNode = tree.getNode(tree.getNodeId(nodeIdIndex));
			
			assertNotNull(selectedNode);
			assertEquals(2,selectedNode.getNodeLevel().intValue());
			
			Integer jailedMafioso = selectedNode.getNodeId();
			
			assertNotNull(jailedMafioso);
			
			System.out.println("Node Selected to Go to Jail *****************");
			System.out.println("*********************************************");
			System.out.println("Name : " + selectedNode.getMafioso().getName());
			System.out.println("Age : "  + selectedNode.getMafioso().getAge());
			System.out.println("Level : " + selectedNode.getMafioso().getLevel());
			System.out.println("Id : " + selectedNode.getNodeId());
			System.out.println("Number Of Children : " + selectedNode.getNumberOfChildren());
			System.out.println("*********************************************");
			System.out.println("");
			System.out.println("");
			
			tree.goingToJail(selectedNode, true);
			
			System.out.println("Tree after Imprisonment *********************");
			System.out.println("*********************************************");			
			tree.printTree();
			System.out.println("*********************************************");
			System.out.println("");
			System.out.println("");
			
			tree.releasedFromPrisonId(jailedMafioso);
			
			System.out.println("Tree after Releasing ************************");
			System.out.println("*********************************************");			
			tree.printTree();
			System.out.println("*********************************************");
			System.out.println("");
			System.out.println("");

		}

	}
	
	@Test
	public void givensOneMafiosoGoingToJailAndOldestInLevelIsTheNewBossAndReleased(){
		
		MafiaMember capo = new MafiaMember();
		boolean enabled = true;
		
		//Givens
		if(enabled){
			capo = new MafiaMember();
			capo.setAge(this.AGE);
			capo.setName(this.NAME);
			//We generate a dummy tree with n levels and n child per node
			MafiaTreeGenerator mafiaTreeGenerator = new MafiaTreeGenerator();
			mafiaTreeGenerator.finalNodeLevel = 2;
			mafiaTreeGenerator.numberOfChildren = 2;
			mafiaTreeGenerator.generateOrganisation();
			Tree tree = mafiaTreeGenerator.testTree;
			
			assertNotNull(tree);
			//
			Tree.Node root = tree.getRoot();
			
			assertNotNull(root);
			
			System.out.println("Test 3 givensOneMafiosoGoingToJailAndOldestInLevelIsTheNewBossAndReleased");
			System.out.println("Initial Tree ********************************");
			System.out.println("*********************************************");			
			tree.printTree();
			System.out.println("*********************************************");
			System.out.println("");
			System.out.println("");
			
			Tree.Node selectedNode = null;
			
			//We select the member of the Mafia that goes to jail randomly
			while(true){
				nodeIdIndex = rn.nextInt(tree.getNumberOfNodes());
		
				System.out.println("Random Index: &&& " + nodeIdIndex + " " + tree.getNodeId(nodeIdIndex) + " " + tree.getNumberOfNodes() + " &&&");
				System.out.println("");
			
				selectedNode = tree.getNode(tree.getNodeId(nodeIdIndex));
				
				//We prefer the selected node to have children so we prefer
				//any level  but the last
				if(selectedNode.getNodeLevel()!=mafiaTreeGenerator.finalNodeLevel)
					break;
			}
			
			assertNotNull(selectedNode);
			
			Integer jailedMafioso = selectedNode.getNodeId();
			
			assertNotNull(jailedMafioso);
			
			System.out.println("Node Selected to Go to Jail *****************");
			System.out.println("*********************************************");
			System.out.println("Name : " + selectedNode.getMafioso().getName());
			System.out.println("Age : "  + selectedNode.getMafioso().getAge());
			System.out.println("Level : " + selectedNode.getMafioso().getLevel());
			System.out.println("Id : " + selectedNode.getNodeId());
			System.out.println("Number Of Children : " + selectedNode.getNumberOfChildren());
			System.out.println("*********************************************");
			System.out.println("");
			System.out.println("");
			
			tree.goingToJail(selectedNode, true);
			
			System.out.println("Tree after Imprisonment *********************");
			System.out.println("*********************************************");			
			tree.printTree();
			System.out.println("*********************************************");
			System.out.println("");
			System.out.println("");
			
			tree.releasedFromPrisonId(jailedMafioso);
			
			System.out.println("Tree after Releasing ************************");
			System.out.println("*********************************************");			
			tree.printTree();
			System.out.println("*********************************************");
			System.out.println("");
			System.out.println("");
		}

	}
	
	@Test
	public void givensOneMafiosoWithMoreThan50Subordinates(){
		MafiaMember capo = new MafiaMember();
		boolean enabled = true;
		
		//Givens
		if(enabled){
			capo = new MafiaMember();
			capo.setAge(this.AGE);
			capo.setName(this.NAME);
			//We generate a dummy tree with n levels and n child per node
			MafiaTreeGenerator mafiaTreeGenerator = new MafiaTreeGenerator();
			mafiaTreeGenerator.finalNodeLevel = 4;
			mafiaTreeGenerator.numberOfChildren = 3;
			mafiaTreeGenerator.generateOrganisation();
			Tree tree = mafiaTreeGenerator.testTree;
			
			assertNotNull(tree);
			//
			Tree.Node root = tree.getRoot();
			
			assertNotNull(root);
			
			boolean isMoreThan50 = root.hasMoreThan50Subordinates();
			
			assertEquals(true, isMoreThan50);
		}
	}
	
	@Test
	public void restructuringOganization(){
		
		MafiaMember capo = new MafiaMember();
		boolean enabled = true;
		
		//Givens
		if(enabled){
			capo = new MafiaMember();
			capo.setAge(this.AGE);
			capo.setName(this.NAME);
			//We generate a dummy tree with n levels and n child per node
			MafiaTreeGenerator mafiaTreeGenerator = new MafiaTreeGenerator();
			mafiaTreeGenerator.finalNodeLevel = 4;
			mafiaTreeGenerator.numberOfChildren = 2;
			mafiaTreeGenerator.generateOrganisation();
			Tree tree = mafiaTreeGenerator.testTree;
			
			assertNotNull(tree);
			//
			Tree.Node root = tree.getRoot();
			
			assertNotNull(root);
			
			System.out.println("Test 5 restructuringOganization**************");
			System.out.println("Initial Tree ********************************");
			System.out.println("*********************************************");			
			tree.printTree();
			System.out.println("*********************************************");
			System.out.println("");
			System.out.println("");
			
			Tree.Node selectedNode = null;
			
			//We select randomly
			Integer origin = null, destiny = null;
			while(true){
				nodeIdIndex = rn.nextInt(tree.getNumberOfNodes());
		
				System.out.println("Random Index: &&& " + nodeIdIndex + " " + tree.getNodeId(nodeIdIndex) + " " + tree.getNumberOfNodes() + " &&&");
				System.out.println("");
			
				selectedNode = tree.getNode(tree.getNodeId(nodeIdIndex));
				
				//We prefer the selected node to have children so we prefer
				//any level  but the last
				if(selectedNode.getNodeLevel()!=mafiaTreeGenerator.finalNodeLevel){
					if(origin==null)
						origin = selectedNode.getNodeId();
					else if(destiny==null&&origin!=selectedNode.getNodeId()&&selectedNode.getNodeId()!=0){
						destiny = selectedNode.getNodeId();
						break;
					}
				}
			}

			
			Integer[] movingMafiosos = {origin};

			assertNotNull(movingMafiosos);
			
			selectedNode= tree.getNode(origin);
			
			System.out.println("Node Selected to be moved *******************");
			System.out.println("*********************************************");
			System.out.println("Name : " + selectedNode.getMafioso().getName());
			System.out.println("Age : "  + selectedNode.getMafioso().getAge());
			System.out.println("Level : " + selectedNode.getMafioso().getLevel());
			System.out.println("Id : " + selectedNode.getNodeId());
			System.out.println("Number Of Children : " + selectedNode.getNumberOfChildren());
			System.out.println("*********************************************");
			System.out.println("");
			System.out.println("");
			
			selectedNode= tree.getNode(destiny);
			
			System.out.println("Node Selected to be moved to ****************");
			System.out.println("*********************************************");
			System.out.println("Name : " + selectedNode.getMafioso().getName());
			System.out.println("Age : "  + selectedNode.getMafioso().getAge());
			System.out.println("Level : " + selectedNode.getMafioso().getLevel());
			System.out.println("Id : " + selectedNode.getNodeId());
			System.out.println("Number Of Children : " + selectedNode.getNumberOfChildren());
			System.out.println("*********************************************");
			System.out.println("");
			System.out.println("");
			
			if(!tree.makeIdsChildrenOfId(movingMafiosos, destiny)){
				System.out.println("");
				System.out.println("");
				System.out.println("Change Not Allowed  **************************");
				System.out.println("*********************************************");
				System.out.println("");
				System.out.println("");
				
			}

			
			System.out.println("Tree after Change ***************************");
			System.out.println("*********************************************");			
			tree.printTree();
			System.out.println("*********************************************");
			System.out.println("");
			System.out.println("");
		}
	}
}
